#!/bin/sh -e

[[ -n "$DEBUG" ]] && set -x

generate_token() {
	(cd ${ASGARD_DIR} && bundle exec rake generate_secret_token)
}

migrate_database() {
	(cd ${ASGARD_DIR} && bundle exec rake db:migrate)
}

app_init() {
	generate_token
	migrate_database
	(cd ${ASGARD_DIR} && touch .init)
}

case ${1} in
  app:start|app:init)
    case ${1} in
      app:start)
        [[ ! -f .init ]] && app_init

        rm -rf /var/run/supervisor.sock
        exec /usr/bin/supervisord -nc /etc/supervisord.conf
        ;;
      app:init)
	app_init
        ;;
    esac
    ;;
  app:help|help)
    echo "Available options:"
    echo " app:start          - Starts the Asgard server (default)"
    echo " app:init           - Initialize the Asgard server"
    echo " app:help           - Display this help message"
    echo " shell              - Launch a shell session"
    echo " [command]          - Execute a command"
    ;;
  shell)
    exec "/bin/ash"
    ;;
  *)
    exec "$@"
    ;;
esac
