worker_processes 1;
pid /var/run/nginx.pid;
error_log /dev/stderr;

events {
	worker_connections 32;
	accept_mutex off;
	use epoll;
}

http {
	include mime.types;
	default_type application/octet-stream;

	access_log /dev/stdout combined;

	sendfile on;

	tcp_nopush on;
	tcp_nodelay off;

	gzip on;
	gzip_http_version 1.0;
	gzip_proxied any;
	gzip_min_length 500;
	gzip_disable "MSIE [1-6]\.";
	gzip_types text/plain text/xml text/css
		text/comma-separated-values
		text/javascript application/x-javascript
		application/atom+xml;

	upstream unicorn {
		server unix:/tmp/asgard.sock fail_timeout=0;
	}

	server {
		listen 80 default deferred;
		listen [::]:80 ipv6only=on deferred;

		client_max_body_size 4G;
		server_name _;

		keepalive_timeout 5;

		root /usr/share/webapps/redmine/public;

		try_files /index.html .html @app;

		location @app {
			proxy_set_header HOST $host;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_redirect off;

			proxy_pass http://unicorn;
		}

		error_page 500 502 503 504 /500.html;

		location = /500.html {
			root /usr/share/webapps/redmine/public;
		}

		error_page 404 /404.html;

		location = /404.html {
			root /usr/share/webapps/redmine/public;
		}
	}
}
